<?php
use iamdev\rest\Context;
use iamdev\rest\exceptions\ResourceNotFound;
use iamdev\rest\Restifier;
use \RedBeanPHP\R as R;

class Playlists implements Restifier
{
    public function list(Context $context): ?array
    {
        return array_values($context->parent->ownPlaylistList);
    }

    public function create(object $resource, Context $context): string
    {
        $user = $context->parent;
        $playlist = R::dispense('playlist');
        $playlist->name = $resource->name;
        $user->ownPlaylistList[] = $playlist;
        R::store($user);
        return $playlist->id;
    }

    public function retrieve($id, Context $context): object
    {
        $playlist = $context->parent->ownPlaylistList[$id];
        if ($playlist) {
            return $playlist;
        }
        throw new ResourceNotFound();
    }

    public function update($id, $resource, Context $context)
    {
        $user = $context->parent;
        if (array_key_exists($id, $user->ownPlaylistList)) {
            $playlist = $user->ownPlaylistList[$id];
            $playlist->name = $resource->name;
            R::store($playlist);
        } else {
            throw new ResourceNotFound();
        }
    }

    public function delete($id, Context $context)
    {
        $user = $context->parent;
        unset($user->ownPlaylistList[$id]);
        R::store($user);
    }

}