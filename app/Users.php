<?php
use iamdev\rest\Context;
use iamdev\rest\exceptions\Conflict;
use iamdev\rest\exceptions\Forbidden;
use iamdev\rest\exceptions\ResourceNotFound;
use iamdev\rest\exceptions\Unauthorized;
use iamdev\rest\Restifier;
use \RedBeanPHP\R as R;

class Users implements Restifier
{
    public function list(Context $context): array
    {
        return array_values(R::findAll("user"));
    }

    public function create(object $resource, Context $context): string
    {
        $user = R::dispense([
            '_type' => 'user',
            'name' => $resource->name,
        ]);
        $id = R::store($user);
        return $id;
    }

    public function retrieve($id, Context $context): object
    {
        if ($id == 'unauthorized') {
            throw new Unauthorized();
        }
        if ($id == 'conflict') {
            throw new Conflict();
        }
        if ($id === 'secret') {
            throw new Forbidden();
        }
        if ($id === 'kurt') {
            $user = new stdClass();
            $user->name = 'Kurt Cobain';
            if (!$context->isLeaf) {
                $user->name = 'kcobain';
            }
            return $user;
        }
        $user = R::load('user', $id);
        if ($user->id) {
            return $user;
        }
        throw new ResourceNotFound();
    }

    public function update($id, $resource, Context $context)
    {
        $user = R::load('user', $id);
        if ($user->id) {
            $user->name = $resource->name;
            R::store($user);
        } else {
            throw new ResourceNotFound();
        }
    }

    public function delete($id, Context $context)
    {
        $user = R::dispense([
            '_type' => 'user',
            'id' => $id,
        ]);
        R::trash($user);
    }

}