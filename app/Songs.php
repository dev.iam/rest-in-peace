<?php
use iamdev\rest\Context;
use iamdev\rest\exceptions\ResourceNotFound;
use iamdev\rest\Restifier;
use \RedBeanPHP\R as R;

class Songs implements Restifier
{
    public function list(Context $context): ?array
    {
        if ($context->parent->name === 'kcobain') {
            $song = new stdClass();
            $song->name = 'In Bloom';
            return [$song];
        }
        $response = array_values($context->parent->ownSongList);

        $songs = [];
        foreach ($response as $value) {
            $songs[] = Song::from($value);
        }
        return $songs;
    }

    public function create(object $resource, Context $context): string
    {
        $userOrPlaylist = $context->parent;
        $song = R::dispense('song');
        $song->name = $resource->name;
        $userOrPlaylist->ownSongList[] = $song;
        R::store($userOrPlaylist);
        return $song->id;
    }

    public function retrieve($id, Context $context): object
    {
        $song = $context->parent->ownSongList[$id];
        if ($song) {
            return Song::from($song);
        }
        throw new ResourceNotFound();
    }

    public function update($id, $resource, Context $context)
    {
        $parent = $context->parent;
        if (array_key_exists($id, $parent->ownSongList)) {
            $song = $parent->ownSongList[$id];
            $song->name = $resource->name;
            R::store($song);
        } else {
            throw new ResourceNotFound();
        }
    }

    public function delete($id, Context $context)
    {
        $parent = $context->parent;
        unset($parent->ownSongList[$id]);
        R::store($parent);
    }

}

class Song
{
    public string $name;
    public string $id;

    public static function from($object)
    {
        $song = new Song();
        $song->id = $object->id;
        $song->name = $object->name;
        return $song;
    }
}

