<?php
use iamdev\rest\handlers\io\IOHandler;
use iamdev\rest\handlers\io\XmlHandler;
use iamdev\rest\Rest;
use \RedBeanPHP\R as R;

require_once './vendor/autoload.php';

include './Users.php';
include './Songs.php';
include './Playlists.php';

R::setup();

class User
{
    public string $name;
}
class MyXmlHandler extends XmlHandler
{
    public function serialize($resource, $name): string
    {
        if ($name === 'users' && is_object($resource)) {
            $user = new User();
            $user->name = $resource->name;
            $resource = $user;
        }
        return parent::serialize($resource, $name);
    }

}

class HtmlHandler implements IOHandler
{
    public function getExtension(): string
    {
        return 'html';
    }

    public function getMimeType(): string
    {
        return 'text/html';
    }

    public function deserialize(string $data)
    {
        throw new RuntimeException('not implmented');
    }

    public function serialize($resource, $name): string
    {
        return "<body>" . json_encode($resource) . "</body>";
    }
}

$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$path = substr($path, 4);
Rest::inPeace(
    [
        'users' => new Users(),
        'songs' => new Songs(),
        'playlists' => new Playlists(),
    ],
    $path,
    [new MyXmlHandler(), new HtmlHandler()]
);
