<?php
use iamdev\rest\Rest;
use \RedBeanPHP\R as R;

require_once './vendor/autoload.php';
include './Users.php';
include './Songs.php';
include './Playlists.php';

R::setup();

Rest::inPeace(
    [
        'users' => new Users(),
        'songs' => new Songs(),
        'playlists' => new Playlists(),
    ],
);
