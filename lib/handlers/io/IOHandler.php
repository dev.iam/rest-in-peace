<?php
namespace iamdev\rest\handlers\io;

interface IOHandler
{
    public function getExtension(): string;
    public function getMimeType(): string;
    public function deserialize(string $data);
    public function serialize($resource, $name): string;
}
