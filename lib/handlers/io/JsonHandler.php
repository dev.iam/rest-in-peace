<?php
namespace iamdev\rest\handlers\io;

class JsonHandler implements IOHandler
{
    public function getExtension(): string
    {
        return 'json';
    }

    public function getMimeType(): string
    {
        return 'application/json';
    }

    public function deserialize(string $data)
    {
        $object = json_decode($data);
        if ($object) {
            return $object;
        }
        throw new \RuntimeException('invalid json');
    }

    public function serialize($resource, $name): string
    {
        return json_encode($resource);
    }
}
