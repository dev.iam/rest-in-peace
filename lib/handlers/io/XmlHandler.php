<?php
namespace iamdev\rest\handlers\io;

use Exception;

class XmlHandler implements IOHandler
{
    public function getExtension(): string
    {
        return 'xml';
    }

    public function getMimeType(): string
    {
        return 'text/xml';
    }

    public function deserialize(string $data)
    {
        $xml = simplexml_load_string($data);
        $object = json_decode(json_encode($xml));
        if ($object) {
            return $object;
        }
        throw new \RuntimeException('invalid xml');
    }

    public function serialize($resource, $name): string
    {
        if (is_array($resource)) {
            $response = new \stdClass();
            $response->$name = $resource;
            return XmlHandler::from($resource, $name);
        }
        return XmlHandler::from($resource);
    }

    // credit : https://stackoverflow.com/a/47074230
    private static function GetClassNameWithoutNamespace($object)
    {
        $class_name = get_class($object);
        $path = explode('\\', $class_name);
        return strtolower(end($path));
    }

    private static function from($object, string $root_node = null)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        if (!$root_node) {
            $root_node = self::GetClassNameWithoutNamespace($object);
        }
        $xml .= '<' . $root_node . '>';
        $xml .= self::SerializeNode($object, false, true);
        $xml .= '</' . $root_node . '>';
        return $xml;
    }

    private static function SerializeNode($node, $parent_node_name = false, $is_array_item = false)
    {
        $xml = '';
        if (is_object($node)) {
            $vars = get_object_vars($node);
        } else if (is_array($node)) {
            $vars = $node;
        } else {
            throw new Exception('Coś poszło nie tak');
        }

        foreach ($vars as $k => $v) {
            if (is_object($v)) {
                $node_name = ($parent_node_name ? $parent_node_name : self::GetClassNameWithoutNamespace($v));
                if (!$is_array_item) {
                    $node_name = $k;
                }
                $xml .= '<' . $node_name . '>';
                $xml .= self::SerializeNode($v);
                $xml .= '</' . $node_name . '>';
            } else if (is_array($v)) {
                $xml .= '<' . $k . '>';
                if (count($v) > 0) {
                    if (is_object(reset($v))) {
                        $xml .= self::SerializeNode($v, self::GetClassNameWithoutNamespace(reset($v)), true);
                    } else {
                        $xml .= self::SerializeNode($v, gettype(reset($v)), true);
                    }
                } else {
                    $xml .= self::SerializeNode($v, false, true);
                }
                $xml .= '</' . $k . '>';
            } else {
                $node_name = ($parent_node_name ? $parent_node_name : $k);
                if ($v === null) {
                    continue;
                } else {
                    $xml .= '<' . $node_name . '>';
                    if (is_bool($v)) {
                        $xml .= $v ? 'true' : 'false';
                    } else {
                        $xml .= htmlspecialchars($v, ENT_QUOTES);
                    }
                    $xml .= '</' . $node_name . '>';
                }
            }
        }
        return $xml;
    }
}
