<?php
namespace iamdev\rest\handlers\io;

use iamdev\rest\exceptions\BadRequest;

class IO
{
    private array $handlers;
    private ?IOHandler $handler = null;

    public function __construct(array $ioHandlers, string $format = null)
    {
        foreach ($ioHandlers as $handler) {
            if ($handler instanceof IOHandler) {
                $this->handlers[] = $handler;
            } else {
                throw new \RuntimeException("one of the handler does not implement IOHandler");
            }
        }
        $this->handlers[] = new JsonHandler();
        $this->handlers[] = new XmlHandler();

        if ($format != null) {
            $this->handler = $this->getHandlerFromExtension($format);
        }
    }

    private function getHandlerFromExtension(string $format): IOHandler
    {
        foreach ($this->handlers as $handler) {
            if ($handler->getExtension() === $format) {
                return $handler;
            }
        }
        throw new BadRequest('no handler for ' . $format);
    }

    private function getHandlerFromHeader(string $name): IOHandler
    {
        $mimeType = $this->getHeader($name);
        foreach ($this->handlers as $handler) {
            if ($handler->getMimeType() === $mimeType) {
                return $handler;
            }
        }
        throw new BadRequest('no handler for ' . $mimeType);
    }

    private function getHeader(string $name): string
    {
        $headers = getallheaders();
        foreach ($headers as $header => $value) {
            if (strcasecmp($header, $name) === 0) {
                return $value;
            }
        }
        throw new BadRequest('no header ' . $name);
    }

    public function content()
    {
        if ($this->handler == null) {
            $this->handler = $this->getHandlerFromHeader('content-type');
        }
        $content = file_get_contents('php://input');
        return $this->handler->deserialize($content);
    }

    public function accept($resource, $name)
    {
        if ($this->handler == null) {
            $this->handler = $this->getHandlerFromHeader('accept');
        }
        header('Content-type: ' . $this->handler->getMimeType());
        echo $this->handler->serialize($resource, $name);
    }
}
