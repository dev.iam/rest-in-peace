<?php
namespace iamdev\rest\handlers\method;

use iamdev\rest\Context;
use iamdev\rest\handlers\io\IO;
use iamdev\rest\Restifier;
use iamdev\rest\RestifierSupplier;

abstract class MethodHandler
{
    protected RestifierSupplier $restifierSupplier;
    protected IO $io;

    public function __construct(RestifierSupplier $supplier, IO $io)
    {
        $this->restifierSupplier = $supplier;
        $this->io = $io;
    }

    public function handle(array $resourceMap)
    {
        $context = new Context($resourceMap);

        foreach ($resourceMap as $resourceName => $id) {
            $context->currentPath .= '/' . $resourceName . '/' . $id;

            $restifier = $this->restifierSupplier->get($resourceName);
            if ($resourceName === $context->targetedResource) {
                $context->isLeaf = true;
                $this->doHandle($restifier, $id, $context);
            } else {
                $context->parent = $restifier->retrieve($id, $context);
            }
        }
    }

    protected abstract function doHandle(Restifier $restifier, string $id, Context $context);
}
