<?php
namespace iamdev\rest\handlers\method;

use iamdev\rest\Context;
use iamdev\rest\Restifier;

class PutHandler extends MethodHandler
{
    protected function doHandle(Restifier $restifier, ?string $id, Context $context)
    {
        $resource = $this->io->content();
        $restifier->update($id, $resource, $context);
        http_response_code(204);
    }
}
