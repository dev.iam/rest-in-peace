<?php
namespace iamdev\rest\handlers\method;

use iamdev\rest\Context;
use iamdev\rest\Restifier;

class PostHandler extends MethodHandler
{
    protected function doHandle(Restifier $restifier, ?string $id, Context $context)
    {
        $resource = $this->io->content();
        $id = $restifier->create($resource, $context);
        header('Location: ' . $context->currentPath . $id);
        http_response_code(201);
    }
}