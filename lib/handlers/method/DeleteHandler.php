<?php
namespace iamdev\rest\handlers\method;

use iamdev\rest\Context;
use iamdev\rest\Restifier;

class DeleteHandler extends MethodHandler
{
    protected function doHandle(Restifier $restifier, ?string $id, Context $context)
    {
        $restifier->delete($id, $context);
        http_response_code(204);
    }
}