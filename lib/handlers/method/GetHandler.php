<?php
namespace iamdev\rest\handlers\method;

use iamdev\rest\Context;
use iamdev\rest\Restifier;

class GetHandler extends MethodHandler
{
    protected function doHandle(Restifier $restifier, ?string $id, Context $context)
    {
        $response = null;
        if ($id) {
            $response = $restifier->retrieve($id, $context);
        } else {
            $response = $restifier->list($context);
        }
        $this->io->accept($response, $context->targetedResource);
    }
}
