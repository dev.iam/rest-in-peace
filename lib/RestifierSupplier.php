<?php
namespace iamdev\rest;

class RestifierSupplier
{
    private array $routes = [];

    public function __construct(array $routes)
    {
        $this->routes = $routes;
    }

    function get($resource): Restifier
    {
        if (array_key_exists($resource, $this->routes)) {
            return $this->routes[$resource];
        }
        throw new \RuntimeException('missing Restifier for resource /' . $resource);
    }
}

