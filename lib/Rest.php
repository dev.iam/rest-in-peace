<?php
namespace iamdev\rest;

use iamdev\rest\exceptions\BadRequest;
use iamdev\rest\exceptions\RestException;
use iamdev\rest\handlers\io\IO;
use iamdev\rest\handlers\method\DeleteHandler;
use iamdev\rest\handlers\method\GetHandler;
use iamdev\rest\handlers\method\PostHandler;
use iamdev\rest\handlers\method\PutHandler;
use Exception;

class Rest
{
    private array $methodHandlers;
    private array $resourceMap = [];

    private function __construct(array $routes, string $path, array $ioHandlers)
    {
        $path = substr($path, 1);
        $pieces = explode('.', $path);
        if (count($pieces) > 2) {
            throw new BadRequest('more than one . found, use it only to indicate format');
        }
        $format = null;
        if (count($pieces) == 2) {
            $path = $pieces[0];
            $format = $pieces[1];
        }

        $this->buildMethodHandlers(new RestifierSupplier($routes), new IO($ioHandlers, $format));
        $this->buildResourseMap($path);
    }

    public static function inPeace(array $routes, $path = null, array $ioHandlers = [])
    {
        try {
            if ($path === null) {
                $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            }
            $rest = new Rest($routes, $path, $ioHandlers);
            $rest->handle();
        } catch (RestException $e) {
            http_response_code($e->status());
            echo $e->getMessage();
        } catch (Exception $e) {
            http_response_code(500);
            echo $e->getMessage();
            error_log($e->getMessage());
            error_log($e->getTraceAsString());
        }
    }

    protected function handle()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if (array_key_exists($method, $this->methodHandlers)) {
            $this->methodHandlers[$method]->handle($this->resourceMap);
        } else {
            throw new BadRequest('unsupported http method');
        }
    }

    private function buildMethodHandlers(RestifierSupplier $supplier, IO $io)
    {
        $this->methodHandlers = [
            "GET" => new GetHandler($supplier, $io),
            "POST" => new PostHandler($supplier, $io),
            "PUT" => new PutHandler($supplier, $io),
            "DELETE" => new DeleteHandler($supplier, $io),
        ];
    }

    private function buildResourseMap(string $path)
    {
        $pieces = explode('/', $path);

        $length = sizeof($pieces);
        for ($i = 0; $i < $length; $i = $i + 2) {
            $resource = $pieces[$i];
            $id = null;
            if ($i + 1 < $length) {
                $id = $pieces[$i + 1];
            }
            $this->resourceMap[$resource] = $id;
        }
    }
}