<?php
namespace iamdev\rest;

interface Restifier
{
    public function create(object $resource, Context $context): string;
    public function retrieve($id, Context $context): object;
    public function update($id, $resource, Context $context);
    public function delete($id, Context $context);
    public function list(Context $context): ?array;
}