<?php
namespace iamdev\rest;

class Context
{
    public ?object $parent = null;
    public array $resourceMap;
    public string $currentPath = '';
    public bool $isLeaf = false;
    public string $targetedResource;
    public function __construct(array $resourceMap)
    {
        $this->resourceMap = $resourceMap;
        $this->targetedResource = array_key_last($resourceMap);
    }
}
