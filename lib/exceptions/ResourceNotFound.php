<?php
namespace iamdev\rest\exceptions;

class ResourceNotFound extends RestException
{
    public function status(): int
    {
        return 404;
    }
}
