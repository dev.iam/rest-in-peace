<?php
namespace iamdev\rest\exceptions;

class Unauthorized extends RestException
{
    public function status(): int
    {
        return 401;
    }
}
