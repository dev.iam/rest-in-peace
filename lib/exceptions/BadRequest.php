<?php
namespace iamdev\rest\exceptions;

class BadRequest extends RestException
{
    public function status(): int
    {
        return 400;
    }
}
