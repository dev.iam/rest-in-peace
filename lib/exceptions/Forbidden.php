<?php
namespace iamdev\rest\exceptions;

class Forbidden extends RestException
{
    public function status(): int
    {
        return 403;
    }
}