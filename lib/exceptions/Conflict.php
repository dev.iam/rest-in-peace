<?php
namespace iamdev\rest\exceptions;

class Conflict extends RestException
{
    public function status(): int
    {
        return 409;
    }
}

