<?php
namespace iamdev\rest\exceptions;

use Exception;

abstract class RestException extends Exception
{
    abstract public function status(): int;
}
