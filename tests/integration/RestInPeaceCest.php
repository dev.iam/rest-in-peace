<?php

class RestInPeaceCest
{
    public function testCreate(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Accept', 'application/json');

        $I->sendPost('/users', ['name' => 'Pepito']);
        $I->seeResponseCodeIs(201);
        $I->seeHttpHeader('Location', '/users/1');

        $I->sendPost('/users', ['name' => 'Juanito']);
        $I->seeResponseCodeIs(201);
        $I->seeHttpHeader('Location', '/users/2');

        $I->sendGet('/users');
        $I->seeResponseCodeIs(200);
        $I->seeResponseContains('[{"id":"1","name":"Pepito"},{"id":"2","name":"Juanito"}]');

        $I->sendGet('/users/2');
        $I->seeResponseContains('{"id":"2","name":"Juanito"}');

        $I->sendPost('/users/2/songs', ['name' => 'Guach Perry']);
        $I->seeResponseCodeIs(201);
        $I->seeHttpHeader('Location', '/users/2/songs/1');

        $I->sendGet('/users/2/songs');
        $I->seeResponseContains('[{"name":"Guach Perry","id":"1"}]');

        $I->sendGet('/users/2/songs/1');
        $I->seeResponseContains('{"name":"Guach Perry","id":"1"}');

        $I->sendPost('/users/1/playlists', ['name' => 'Verano 2024']);
        $I->seeResponseCodeIs(201);
        $I->seeHttpHeader('Location', '/users/1/playlists/1');

        $I->sendPost('/users/1/playlists/1/songs', ['name' => 'Sur o no Sur']);
        $I->seeResponseCodeIs(201);
        $I->seeHttpHeader('Location', '/users/1/playlists/1/songs/2');

        $I->sendGet('/users/1/playlists/1/songs');
        $I->seeResponseContains('[{"name":"Sur o no Sur","id":"2"}]');

        $I->sendGet('/users/1/playlists/1/songs/2');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"name":"Sur o no Sur","id":"2"}');
    }

    public function testUpdate(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Accept', 'application/json');

        $I->sendPut('/users/1', ['name' => 'Pepito Pepon']);
        $I->seeResponseCodeIs(204);

        $I->sendGet('/users/1');
        $I->seeResponseContains('{"id":"1","name":"Pepito Pepon"}');

        $I->sendPut('/users/1/playlists/1', ['name' => 'Verano 2023']);
        $I->seeResponseCodeIs(204);

        $I->sendGet('/users/1/playlists/1');
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"id":"1","name":"Verano 2023","user_id":"1"}');

        $I->sendPut('/users/1/playlists/1/songs/2', ['name' => 'Sur o No Sur']);
        $I->seeResponseCodeIs(204);

        $I->sendGet('/users/1/playlists/1/songs/2');
        $I->seeResponseContains('{"name":"Sur o No Sur","id":"2"}');
    }
    public function testDelete(ApiTester $I)
    {
        $I->sendDelete('/users/2/songs/1');
        $I->seeResponseCodeIs(204);

        $I->sendGet('/users/2/songs/1');
        $I->seeResponseCodeIs(404);

        $I->sendDelete('/users/2');
        $I->seeResponseCodeIs(204);

        $I->sendGet('/users/2');
        $I->seeResponseCodeIs(404);

        $I->sendDelete('/users/1/playlists/1/songs/2');
        $I->seeResponseCodeIs(204);

        $I->sendGet('/users/1/playlists/1/songs/2');
        $I->seeResponseCodeIs(404);
    }

    public function testErrors(ApiTester $I)
    {
        $I->sendGet('/users');
        $I->seeResponseCodeIs(400);
        $I->seeResponseContains('no header accept');

        $I->haveHttpHeader('Content-Type', 'application/json');

        $I->sendPut('/users/2', ['name' => 'Unknown']);
        $I->seeResponseCodeIs(404);

        $I->sendPut('/users/1/playlists/2', ['name' => 'Unknown']);
        $I->seeResponseCodeIs(404);

        $I->sendPut('/users/1/playlists/1/songs/3', ['name' => 'Unknown']);
        $I->seeResponseCodeIs(404);

        $I->sendGet('/unknown');
        $I->seeResponseCodeIs(500);

        $I->sendPut('/users/2', '<dad]');
        $I->seeResponseCodeIs(500);
        $I->seeResponseContains('invalid json');

        $I->sendGet('/users/secret');
        $I->seeResponseCodeIs(403);

        $I->sendGet('/users/unauthorized');
        $I->seeResponseCodeIs(401);

        $I->sendGet('/users/conflict');
        $I->seeResponseCodeIs(409);

        $I->haveHttpHeader('Accept', 'not/supported');
        $I->sendGet('/users/1');
        $I->seeResponseCodeIs(400);
        $I->seeResponseContains('no handler for not/supported');

        $I->haveHttpHeader('Content-Type', 'not/supported');
        $I->sendPost('/users', ['name' => 'Unknown']);
        $I->seeResponseCodeIs(400);
        $I->seeResponseContains('no handler for not/supported');

        $I->sendOptions('');
        $I->seeResponseCodeIs(400);
        $I->seeResponseContains('unsupported http method');
    }

    public function testIsLeaf(ApiTester $I)
    {
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendGet('/users/kurt');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"name":"Kurt Cobain"}');

        $I->sendGet('/users/kurt/songs');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContains('[{"name":"In Bloom"}]');
    }

    public function testXML(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'text/xml');
        $I->haveHttpHeader('Accept', 'text/xml');

        $I->sendPost('/users/1/songs', '<song><name>Eleanor Rigby</name></song>');
        $I->seeResponseCodeIs(201);
        $I->seeHttpHeader('Location', '/users/1/songs/3');

        $I->sendGet('/api/users/1/songs/3');
        $I->seeResponseCodeIs(200);
        $I->seeResponseContains('<?xml version="1.0" encoding="UTF-8" ?><song><name>Eleanor Rigby</name><id>3</id></song>');

        $I->sendGet('/api/users/1');
        $I->seeResponseCodeIs(200);
        $I->seeResponseContains('<?xml version="1.0" encoding="UTF-8" ?><user><name>Pepito Pepon</name></user>');

    }

    public function testNoHeader(ApiTester $I)
    {
        $I->sendPost('/users/1/songs.xml', '<song><name>Naturaleza Muerta</name></song>');
        $I->seeResponseCodeIs(201);
        $I->seeHttpHeader('Location', '/users/1/songs/4');

        $I->sendGet('/api/users/1/songs/4.html');
        $I->seeResponseCodeIs(200);
        $I->seeHttpHeader('Content-type', 'text/html;charset=UTF-8');
        $I->seeResponseContains('<body>{"name":"Naturaleza Muerta","id":"4"}</body>');

        $I->sendGet('/api/users/1/songs/4.unknown');
        $I->seeResponseCodeIs(400);
        $I->seeResponseContains('no handler for unknown');

        $I->sendGet('/api/users/1.json/songs/4.unknown');
        $I->seeResponseCodeIs(400);
        $I->seeResponseContains('more than one . found, use it only to indicate format');
    }
}
