service apache2 start
php vendor/bin/codecept run --steps
if [ $? = 1 ]; then
  cat /tmp/PHP_errors.log
  exit 1;
fi
