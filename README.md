# REST in peace

A simple way to provide a REST api on php projects. Follows general directives from https://www.restapitutorial.com/lessons/restquicktips.html.

## How simple?

well,

```php
use iamdev\rest\Rest;

Rest::inPeace(
    [
        'users' => new Users(),
        'songs' => new Songs(),
        'playlists' => new Playlists(),
    ],
);
```

This code will handle routes like `/users/1/songs` or `/users/3/playlists/2/songs` by simply implementing [Restifier](lib/Restifier.php)

```php
use iamdev\rest\Context;
use iamdev\rest\Restifier;

class Playlists implements Restifier
{
    public function list(Context $context): ?array
    {
        //return a list ;
    }

    public function create(object $resource, Context $context): string
    {
        //create a $resource 
        //return the new id;
    }

    public function retrieve($id, Context $context): object
    {
        // return a resource with id $id
    }

    public function update($id, $resource, Context $context)
    {
        // update the $resource
    }

    public function delete($id, Context $context)
    {
        // delete $id
    }
}
```

see this [app](app/) for more complete implementations

## Installation

`php composer.phar require "iamdev/rest"`

## Handling nested routes

`GET /users/3/playlists/2/songs` will call in order:

* Users.php#retrieve(3)
* Playlists.php#retrieve(2)
* Songs.php#list()

Let's say you are using lazy loading and don't want to call 3 times your database, you can take advantage of [Context](lib/Context.php): 
* isLeaf indicates if the restifier is at the end of the route, so, in the example when on Users.php, it will be false and true on Songs.php
* resourceMap indicates the differents parts of the routes, in this case it will contain
```php
array(3) {
  ["users"]=> "3"
  ["playlists"]=> "2"
  ["songs"]=>  NULL
}
```
see [Users.php](app/Users.php)

## Options

Parameters for Rest::inPeace are `(array $routes, $path = null, array $ioHandlers = [])`

`$path` By default, path is resolved as `parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)`, if this doesn't work for you, use this parameter.

`$ioHandlers` is an array of objects implementing [IOHandler](lib/handlers/io/IOHandler.php). By default, there are 2 implementations for [json](lib/handlers/io/JsonHandler.php) an [XML](lib/handlers/io/XmlHandler.php). If you need to replace it or add a new format use this parameter.

see this [example](app/with_options.php) using this options

## Notes

In order to use the default route strategy, you will need rewrite your urls, like for apache

```apache
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^api(.*)$ with_options.php [L,QSA]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.*)$ simple.php [L,QSA]
```

If not, your routes would need to be like `index.php/users/1` and you will have to delete the `index.php` before calling Rest::inPeace

## Exceptions

Simply throw these exceptions to handle http status response:
* 400 - [iamdev\rest\exceptions\BadRequest](lib/exceptions/BadRequest.php)
* 401 - [iamdev\rest\exceptions\Unauthorized](lib/exceptions/Unauthorized.php)
* 403 - [iamdev\rest\exceptions\Forbidden](lib/exceptions/Forbidden.php)
* 404 - [iamdev\rest\exceptions\ResourceNotFound](lib/exceptions/ResourceNotFound.php)
* 409 - [iamdev\rest\exceptions\Conflict](lib/exceptions/Conflict.php)

