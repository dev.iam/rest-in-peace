#docker build -t rest-test --file Dockerfile.it .
docker run --rm  \
  -v "$PWD"/app:/var/www/html \
  -v "$PWD"/tests:/tmp/tests \
  -v "$PWD"/lib:/tmp/rest/lib \
  -v "$PWD"/composer.json:/tmp/rest/composer.json \
  --add-host="host.docker.internal:host-gateway" \
  rest-test