#docker build -t php-7.4-apache-x --file Dockerfile.local .
docker run --rm -p 8081:80 \
  --name rest \
  -v "$PWD"/app:/var/www/html \
  -v "$PWD"/lib:/tmp/rest/lib \
  -v "$PWD"/composer.json:/tmp/rest/composer.json \
  --add-host="host.docker.internal:host-gateway" \
  php:7.4-apache-x